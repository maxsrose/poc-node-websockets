import {Server as HttpServer} from "node:http";
import {Server} from "socket.io";

export const createWsServer = (http: HttpServer): Server => {
    const io = new Server(http);

    io.on("connection", (socket) => {
        console.log("User connected :)");

        socket.on("join", async (data: { playerId: number }) => {
            await socket.join(`${data.playerId}`);

            console.log(socket.rooms);
        })

        socket.on("disconnect", () => {
            for(const room in new Set(socket.rooms))
                socket.leave(room);

            console.log("User disconnected", socket.rooms);
        })
    })

    return io;
}