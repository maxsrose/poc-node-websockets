import express, {Response} from "express";
import {Server as HttpServer} from "node:http";
import * as console from "console";
import {createWsServer} from "./ws-server";
import {connectToAmq} from "./message-consumer";

const app = express();
app.set("port", 3000);

const http = new HttpServer(app);

const wsServer = createWsServer(http);
connectToAmq(wsServer).then();

app.get("/", (_: any, res: Response) =>  {
    res.send("Hello World!");
});

http.listen(3000, () => {
    console.log("Started!");
})