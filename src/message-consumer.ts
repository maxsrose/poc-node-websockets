import {Server} from "socket.io";
import amqp from "amqplib";

export const connectToAmq = async (wsServer: Server) => {
    console.log("Connecting to Rabbit");

    const connection = await amqp.connect("amqp://guest:guest@localhost:5672");
    const channel = await connection.createChannel();

    const q = await channel.assertQueue("player-updates", {  autoDelete: true });
    await channel.bindQueue(q.queue, "player-updates", "balance.update");

    await channel.consume(q.queue, (message) => {
        const messageData = JSON.parse(message?.content?.toString() ?? "") as { playerId: number, [l: string]: unknown };

        console.log("Got message", messageData);

        if(!messageData)
            return;

        wsServer.in(`${messageData.playerId}`).emit("update", messageData);
    })
}